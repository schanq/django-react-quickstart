import React from 'react';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';

const handleSubmit = (event) =>
{
    const name = document.querySelector('#name-input').value;
    axios.get(`/helloworld?name=${name}`).then(({data}) =>
    {
        document.querySelector('#greeting').textContent = data.greeting;
    }).catch(err => console.log(err));
};

const App = () =>
{
    return (
        <div className="App">
            <h1>Django-React Quickstart Application</h1>
            <div>
                <label htmlFor='name-input'> Enter your name: </label>
                <input id='name-input' type='text'/>
                <button onClick={handleSubmit}> Go </button>
            </div>
            <div>
                <p id="greeting"></p>
            </div>
        </div>
      );
};

export default App;
