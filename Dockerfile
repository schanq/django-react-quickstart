FROM node:12.14 as node-deps
WORKDIR /frontend
COPY ./frontend/package.json ./frontend/yarn.lock ./
RUN yarn
COPY ./frontend /frontend
RUN yarn build
FROM python:3.8
WORKDIR /app/backend
COPY ./backend/requirements.txt /app/backend/
RUN pip3 install --upgrade pip -r requirements.txt
COPY . /app/
COPY --from=node-deps /frontend/build /app/frontend/build
WORKDIR /app/frontend/build
RUN mkdir root && mv *.ico *.js *.json root
RUN mkdir /app/staticfiles
WORKDIR /app
RUN DJANGO_SETTINGS_MODULE=api.settings.production \
        SECRET_KEY=somethingsupersecret \
        python3 backend/manage.py collectstatic --noinput
EXPOSE 80
CMD python3 backend/manage.py runserver 0.0.0.0:80
