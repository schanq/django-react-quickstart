from django.http import JsonResponse

def hello_world(request):
    return JsonResponse({'greeting': f'Hello, {request.GET.get("name","")}!'})
