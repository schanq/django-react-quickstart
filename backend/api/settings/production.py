import os
from api.settings.base import *

SECRET_KEY = os.environ.get('SECRET_KEY')
DEBUG = False
ALLOWED_HOSTS = [os.environ.get('PRODUCTION_HOST')]

INSTALLED_APPS.extend(["whitenoise.runserver_nostatic"])

MIDDLEWARE.insert(1, "whitenoise.middleware.WhiteNoiseMiddleware")

TEMPLATES[0]["DIRS"] = [os.path.join(BASE_DIR, "../", "frontend", "build")]

STATICFILES_DIRS = [os.path.join(BASE_DIR, "../", "frontend", "build", "static")]
STATICFILES_STORAGE = "whitenoise.storage.CompressedManifestStaticFilesStorage"
STATIC_ROOT = os.path.join(BASE_DIR, "staticfiles")

STATIC_URL = "/static/"
WHITENOISE_ROOT = os.path.join(BASE_DIR, "../", "frontend", "build", "root")

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': os.environ.get ('DB_NAME'),
        'USER': os.environ.get ('DB_USER'),
        'PASSWORD': os.environ.get ('DB_PASSWORD'),
        'HOST': os.environ.get ('DB_HOST'),
        'PORT': os.environ.get ('DB_PORT', 5432),
    }
}
