.PHONY: \
	startapp \
	run-dev

run-dev: _docker_compose_down _run_dev

run-staging: _docker_compose_down _run_staging
	$(call log,"Running staging environment")
	docker-compose -f docker-compose-staging.yml up

build-run-staging: _docker_compose_down _build_staging _run_staging
	$(call log,"Running staging environment")
	docker-compose -f docker-compose-staging.yml build
	docker-compose -f docker-compose-staging.yml up

build-run-dev: _docker_compose_down _build_dev _run_dev
	$(call log,"Running development docker stack")
	docker-compose -f docker-compose-dev.yml build
	docker-compose -f docker-compose-dev.yml up

migrate:
	$(call log,'Migrating database')
	docker-compose run --rm backend python3 manage.py migrate

makemigrations:
	$(call log,'Making database migrations')
	docker-compose run --rm backend python3 manage.py makemigrations

startapp: _check_app_name
	$(call log,"Creating new django app '${NAME}'")
	docker-compose run --rm backend python3 manage.py startapp ${NAME}

pip-install: _check_deps
	$(call log,"Adding new python dependencies '${DEPS}'")
	docker-compose run --rm backend pip3 install ${DEPS}

yarn-add: _check_deps
	$(call log,"Adding new node dependencies '${DEPS}'")
	docker-compose run --rm frontend yarn add ${DEPS}

_check_app_name:
ifeq ($(strip $(NAME)),)
	echo 'You must provide a name for the app: e.g. `make startapp NAME=my_app`'
	exit 1
endif

_check_deps:
ifeq ($(strip $(DEPS)),)
	echo 'You must provide a whitespace delimited list of dependencies to install: e.g. `make startapp DEPS="a b c"`'
	exit 1
endif

_docker_compose_down:
	$(call log,"Killing docker stack")
	docker-compose -f docker-compose-dev.yml down
	docker-compose -f docker-compose-staging.yml down

_run_dev: _docker_compose_down
	$(call log,"Running development environment")
	docker-compose -f docker-compose-dev.yml up

_run_staging: _docker_compose_down
	$(call log,"Running staging environment")
	docker-compose -f docker-compose-staging.yml up

_build_dev: _docker_compose_down
	$(call log,"Building development environment")
	docker-compose -f docker-compose-dev.yml build

_build_staging: _docker_compose_down
	$(call log,"Building staging environment")
	docker-compose -f docker-compose-staging.yml build

bold := $(shell tput bold)
red := $(shell tput setaf 1)
sgr0 := $(shell tput sgr0)
define log
	@printf "$(bold)$(shell python3 -c \
		"import os;\
		import sys;\
		_,c=os.popen('stty size','r').read().split();\
		print (f' {sys.argv[1]} '.center(int(c), '='))" $1)$(sgr0)"
endef
